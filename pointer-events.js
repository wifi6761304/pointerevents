const screen = document.getElementById('screen');
const output = document.querySelector('output');

let startX = 0;
let endX = 0;
const deadZone = 30;

function onPointerDown(e) {
	startX = e.clientX;
}

// function onPointerMove(e) {
// 	endX = e.clientX;
// }

function onPointerUp(e) {
	endX = e.clientX;
	// Differenz berechnen
	const deltaX = endX - startX;

	// Ganzzahlige Einheiten vergleichen
	if (Math.abs(deltaX) > deadZone) {
		if (deltaX > 0) {
			output.value = "Swipe to the right";
		}
		else {
			output.value = "Swipe to the left";
		}
	}

	startX = 0;
	endX = 0;
}

screen.addEventListener('pointerdown', onPointerDown);
// screen.addEventListener('pointermove', onPointerMove);
screen.addEventListener('pointerup', onPointerUp);
