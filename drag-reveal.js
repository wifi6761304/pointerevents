const todos = document.querySelectorAll(".todo");

const deadZone = 30;
let startX = 0;
let btnMargin = 15;
let currentTodo;
// Wieviel wurde bewegt?
let deltaX = 0;
let lastX = 0;

// Geste gestartet?
let gestureStarted = false;
// Anfangsbreite eines Eltern Elements eines todos
let parentWidth;
// Startbreite des delete Buttons
let deleteButtonWidth;

function onPointerDown(e) {
	// ändert sich nicht
	startX = e.clientX;
	// ändert sich bei jedem move event
	lastX = e.clientX;
	currentTodo = e.target;
	// Um Resourcen zu sparen, fügen wir den Listener erst hinzu, wenn die Geste gestartet wurde
	currentTodo.addEventListener("pointermove", onPointerMove);
	gestureStarted = true;
	// Maximalbreite wird vom Eltern Element bestimmt
	parentWidth = currentTodo.parentElement.getBoundingClientRect().width;
	// Delete Button ist immer ein Geschwisterelement. 1 Möglichkeit: zuerst das Eltern Element
	// selektieren, dann den Button darin selektieren.
	const deleteButton = currentTodo.parentElement.querySelector(".btn-delete");
	deleteButtonWidth = deleteButton.getBoundingClientRect().width;
}

// for animated reveal of the delete button
function onPointerMove(e) {console.log('move');

	deltaX = e.clientX - lastX;
	// wir merken uns die aktuele Position des Zeigers
	lastX = e.clientX;
	// Breite des Todo: die aktuelle Breite + deltaX
	const currentWidth = Math.round(currentTodo.getBoundingClientRect().width + deltaX);

	/*
		Mindestbreite (Start Breite - Button Breite (-buffer))
		Maximalbreite Start Breite
	*/
	if (
		currentWidth >= parentWidth - deleteButtonWidth - btnMargin &&
		currentWidth <= parentWidth
	) {
		currentTodo.style.width = `${currentWidth}px`;
	}
}

function onPointerUp(e) {
	currentTodo.classList.add('todo-transition');
	// wenn die Geste beendet wurde, entfernen wir den Listener
	currentTodo.removeEventListener("pointermove", onPointerMove);

	// let currentWidth = currentTodo.clientWidth;
	let currentWidth = currentTodo.getBoundingClientRect().width;
	if (currentWidth > parentWidth - deleteButtonWidth * 0.5) {
		currentWidth = parentWidth;
	} else {
		currentWidth = parentWidth - deleteButtonWidth - btnMargin;
	}

	currentTodo.style.width = `${currentWidth}px`;

	// reset
	startX = 0;
	currentTodo = null;
	gestureStarted = false;
	parentWidth = null;
	deltaX = 0;
	lastX = 0;
}

todos.forEach((todo) => {
	todo.addEventListener("pointerdown", onPointerDown);
	todo.addEventListener("pointerup", onPointerUp);
	// Muss wieder entfernt werden, da sonst das Ziehen nicht mehr funktioniert
	todo.addEventListener('transitionend', () => {
		todo.classList.remove('todo-transition');
	});
});
