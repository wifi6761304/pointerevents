const todos = document.querySelectorAll('.todo');

const deadZone = 30;
let startX = 0;
let endX = 0;
let movedX = 0;
let currentTodo;
let gestureStarted = false;

function onPointerDown(e) {
	startX = e.clientX;
	currentTodo = e.target;
	gestureStarted = true;
}

// for animated reveal of the delete button
function onPointerMove(e) {
	if (!gestureStarted) return;

	movedX = e.clientX - startX;
}

function onPointerUp(e) {
	endX = e.clientX;
	const deltaX = endX - startX;

	// positive Ganzzahlige Einheiten vergleichen
	if (Math.abs(deltaX) > deadZone) {
		if (deltaX < 0) {
			console.log("Show Delete!", currentTodo);
			currentTodo.closest('li').classList.add('active')

		}
	}

	startX = 0;
	endX = 0;
	currentTodo = null;
	gestureStarted = false;
}

todos.forEach(todo => {
	todo.addEventListener('pointerdown', onPointerDown);
	todo.addEventListener('pointermove', onPointerMove);
	todo.addEventListener('pointerup', onPointerUp);
})